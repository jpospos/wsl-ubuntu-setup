#!/usr/bin/env bash

# run as root
cd /tmp

# Set unlimited bash history
sed -i 's/^HISTSIZE.*/HISTSIZE=-1/' ~/.bashrc
sed -i 's/^HISTFILESIZE.*/HISTFILESIZE=-1/' ~/.bashrc

# https://github.com/microsoft/vscode/issues/85778
cat > /etc/wsl.conf <<EOF
[automount]
enabled=true
root = /
options="metadata,uid=1000,gid=1000,umask=002,dmask=002,fmask=002"
EOF

# Install stuff required for adding repos
apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common

# Installing ansible repo
apt-add-repository ppa:ansible/ansible

# Installing azure-cli repo
curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/azure-cli.list

# Installing docker apt repo
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Install kubectl repo
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list

# Install nodejs repo
curl -sL https://deb.nodesource.com/setup_10.x | bash -

# Update & upgrade system
apt update -y && apt upgrade -y

# Install ansible, docker-ce, kubectl, nodejs, python, python3-pip
apt install -y \
    ansible \
    azure-cli \
    docker-ce \
    fonts-font-awesome \
    fonts-powerline \
    git \
    gource \
    htop \
    kubectl \
    nodejs \
    python3 \
    python3-pip \
    ubuntu-restricted-extras

# Configure non-intrusive nodejs
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo -e 'export PATH=~/.npm-global/bin:$PATH\n' >> ~/.bashrc
source ~/.bashrc
cd /tmp

# Configure docker client to connect to Docker Desktop running on your Windows machine
echo -e "export DOCKER_HOST='tcp://0.0.0.0:2375'\n" >> ~/.bashrc
source ~/.bashrc

# Configure git for longer credential cache-ing
git config --global credential.helper 'cache --timeout 32400'

# Install oh-my-bash (Optional)
sh -c "$(wget https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -O -)"
# Configure this according to your preferences check thier docs

# Install helm@2
wget https://get.helm.sh/helm-v2.11.0-linux-amd64.tar.gz
tar -zxvf helm-v2.11.0-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
rm -rf linux-amd64

# Install terraform
wget https://releases.hashicorp.com/terraform/0.12.26/terraform_0.12.26_linux_amd64.zip
unzip terraform_0.12.26_linux_amd64.zip
mv terraform /usr/local/bin/terraform

# Install terragrunt
wget https://github.com/gruntwork-io/terragrunt/releases/download/v0.23.2/terragrunt_linux_amd64
mv terragrunt_linux_amd64 /usr/local/bin/terragrunt

# Install packer
wget https://releases.hashicorp.com/packer/1.5.4/packer_1.5.4_linux_amd64.zip
unzip packer_1.5.4_linux_amd64.zip
mv packer /usr/local/bin/packer

# Install terminalizer
npm install -g terminalizer

# Install vault
wget https://releases.hashicorp.com/vault/1.3.2/vault_1.3.2_linux_amd64.zip
unzip vault_1.3.2_linux_amd64.zip
mv vault /usr/local/bin/vault

# Create WSLShare for easy file transfer between windows and ubuntu quest
mkdir /mnt/d/WSLShare
ln -s /mnt/d/WSLShare ~/WSLShare

# Create your Workspace
mkdir ~/Workspace