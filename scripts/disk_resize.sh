#!/usr/bin/env bash

read -p 'Disk: ' diskvar
read -p 'Partition No: ' partvar

fdisk $diskvar <<EOF
d
$partvar
n
p
$partvar


w
EOF

xfs_growfs $diskvar$partvar
partprobe $diskvar