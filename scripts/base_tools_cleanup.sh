#!/bin/bash
set -ex

helm delete $(helm list --short -n pipeline --kubeconfig ./kubeconfig) -n pipeline --kubeconfig ./kubeconfig
helm delete $(helm list --short -n observability --kubeconfig ./kubeconfig) -n observability --kubeconfig ./kubeconfig
helm delete $(helm list --short -n kube-tower --kubeconfig ./kubeconfig) -n kube-tower --kubeconfig ./kubeconfig
helm delete $(helm list --short -n kube-system --kubeconfig ./kubeconfig) -n kube-system --kubeconfig ./kubeconfig
kubectl --kubeconfig ./kubeconfig delete namespace pipeline
kubectl --kubeconfig ./kubeconfig delete namespace observability
kubectl --kubeconfig ./kubeconfig delete namespace kube-tower
kubectl --kubeconfig ./kubeconfig delete crd/azurekeyvaultsecrets.spv.no