# WSL Ubuntu Setup #

Collection of scripts to setup WSL Ubuntu easily.

The goal is to be able to do all work natively on WSL.

## What is this repository for ##

This repository documents steps to setup and configure Windows10 with WSL and Ubuntu up to useable state.

This essentially let's you do all your day to day dev and operations work inside your WSL environment.

## Day to day workflow ##
1. Open WSL.exe via cmder or your terminal of choice.
2. Run `code .` (this will run vscode in WSL mode. which will have access to your windows and ubuntu filesystems.)
3. Enjoy.
