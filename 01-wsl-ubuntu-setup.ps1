# RunAs Administrator
# Download Ubuntu 16.04 LTS
Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1604 -OutFile Ubuntu.appx -UseBasicParsing

# Unblock Installer
Unblock-File -Path .\Ubuntu.appx

# Install Ubuntu 16.04 LTS
Add-AppxPackage .\Ubuntu.appx

# This will prompt for initial Unix user:pass creation.