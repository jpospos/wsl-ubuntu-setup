# Configure choco
choco feature enable -n allowGlobalConfirmation

# Install Docker Desktop
choco install docker-desktop --pre -y

# Ensure that on Docker Desktop you:
# - Enable `Expose daemon on tcp://localhost:2375 without TLS`.
# - Enable Start Docker Desktop at startup.
# - Ensure you restart Docker Desktop service.

# Install fonts-powerline
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest -Uri https://github.com/powerline/fonts/archive/master.zip -OutFile .\fonts-powerline.zip
Expand-Archive -Path '.\fonts-powerline.zip' -destinationpath '.\fonts-powerline'
Set-Location ".\fonts-powerline\fonts-master"
Invoke-Expression -Command .\install.ps1

# Install fluent-terminal (does not work on W10 - 1803)
# choco install fluent-terminal -y

# Install cmder
choco install cmder -y --execution-timeout 5400

# Install messaging tools
choco install whatsapp -y
choco install slack -y
